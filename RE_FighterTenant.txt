Version 1
SubGoalCombiner SGC_AND
INITSECTION
DB_HasStoryEvent((ITEMGUID)ITEMGUID_S_RE_SpiderEgg_54f09e4b-ca1a-4a91-b536-c4ba45bca242,"RE_HasSpiderEgg");
DB_GiveItemToEvent((ITEMGUID)ITEMGUID_S_RE_SpiderEgg_54f09e4b-ca1a-4a91-b536-c4ba45bca242,"RE_GiveSpiderEgg");

SetOnStage(CHARACTERGUID_S_RE_PetSpider_0a6cbd9f-a8c5-4cbe-9f31-5a06d8db2554,0);
KBSECTION
//REGION Hatching the egg
// we don't want Biter to hatch during the battle with spiders, only after the egg was picked and then dropped
IF
ItemAddedToCharacter(ITEMGUID_S_RE_SpiderEgg_54f09e4b-ca1a-4a91-b536-c4ba45bca242,_Player)
AND
DB_IsPlayer(_Player)
THEN
ObjectSetFlag(_Player,"QuestUpdate_RE_FighterTenant_FoundSpiderEgg");

IF
ObjectFlagSet("RE_FighterTenantGotSpiderEgg",(CHARACTERGUID)_Player,_)
AND
DB_IsPlayer(_Player)
THEN
ItemToInventory(ITEMGUID_S_RE_SpiderEgg_54f09e4b-ca1a-4a91-b536-c4ba45bca242, CHARACTERGUID_S_RE_FighterTenant_42c6df62-f5c8-451d-a703-abef33e2072f);
SetVarInteger(CHARACTERGUID_S_RE_FighterTenant_42c6df62-f5c8-451d-a703-abef33e2072f,"StatusReApplyOnRemove",0);
RemoveStatus(CHARACTERGUID_S_RE_FighterTenant_42c6df62-f5c8-451d-a703-abef33e2072f,"ENRAGED");
ObjectSetFlag(CHARACTERGUID_S_RE_FighterTenant_42c6df62-f5c8-451d-a703-abef33e2072f,"GotSpiderEgg");

IF
AttackedByObject((ITEMGUID)ITEMGUID_S_RE_SpiderEgg_54f09e4b-ca1a-4a91-b536-c4ba45bca242,(CHARACTERGUID)_Char,_,_,_)
AND
QRY_AttackedByPlayerOrFighter(_Char)
AND
DB_IsPlayer(_Player)
AND
ObjectGetFlag(_Player,"QuestUpdate_RE_FighterTenant_FoundSpiderEgg",1)
AND
GetPosition(ITEMGUID_S_RE_SpiderEgg_54f09e4b-ca1a-4a91-b536-c4ba45bca242,_X,_Y,_Z)
THEN
SetOnStage(ITEMGUID_S_RE_SpiderEgg_54f09e4b-ca1a-4a91-b536-c4ba45bca242,0);
ObjectClearFlag(ITEMGUID_S_RE_SpiderEgg_54f09e4b-ca1a-4a91-b536-c4ba45bca242,"Hatched");
PlayEffectAtPosition("RS3_FX_GP_ScriptedEvent_Teleport_GenericSmoke_01",_X,_Y,_Z);
TeleportTo(CHARACTERGUID_S_RE_PetSpider_0a6cbd9f-a8c5-4cbe-9f31-5a06d8db2554,ITEMGUID_S_RE_SpiderEgg_54f09e4b-ca1a-4a91-b536-c4ba45bca242,"",1);
SetOnStage(CHARACTERGUID_S_RE_PetSpider_0a6cbd9f-a8c5-4cbe-9f31-5a06d8db2554,1);
ObjectSetFlag(CHARACTERGUID_S_RE_PetSpider_0a6cbd9f-a8c5-4cbe-9f31-5a06d8db2554,"Hatched");

QRY
QRY_AttackedByPlayerOrFighter((CHARACTERGUID)_Char)
AND
DB_Tenants(_Char)
THEN
DB_NOOP(1);

QRY
QRY_AttackedByPlayerOrFighter(_Char)
AND
DB_IsPlayer(_Char)
THEN
DB_NOOP(1);

IF
ObjectFlagSet("RE_SpiderPet_Alone",(CHARACTERGUID)CHARACTERGUID_S_RE_PetSpider_0a6cbd9f-a8c5-4cbe-9f31-5a06d8db2554,_)
THEN
SetFaction(CHARACTERGUID_S_RE_PetSpider_0a6cbd9f-a8c5-4cbe-9f31-5a06d8db2554,"Evil NPC");
Proc_StartDialog(1,"RE_AD_FighterTenant_GoneMad",CHARACTERGUID_S_RE_FighterTenant_42c6df62-f5c8-451d-a703-abef33e2072f);

IF
ObjectFlagSet("RE_SpiderPet_BondedWithPlayer",(CHARACTERGUID)CHARACTERGUID_S_RE_PetSpider_0a6cbd9f-a8c5-4cbe-9f31-5a06d8db2554,_)
AND
DB_IsPlayer(_Player)
THEN
DB_BondedSpider((CHARACTERGUID)CHARACTERGUID_S_RE_PetSpider_0a6cbd9f-a8c5-4cbe-9f31-5a06d8db2554);
CharacterAddToPlayerCharacter(CHARACTERGUID_S_RE_PetSpider_0a6cbd9f-a8c5-4cbe-9f31-5a06d8db2554,_Player);
DB_Dialogs(CHARACTERGUID_S_RE_PetSpider_0a6cbd9f-a8c5-4cbe-9f31-5a06d8db2554,"RE_SpiderPet_BondedWithPlayer");
ObjectSetFlag(_Player,"QuestUpdate_RE_FighterTenant_PlayerHatchedSpider");
Proc_StartDialog(1,"RE_AD_SpiderPet_HatchedComment",CHARACTERGUID_S_RE_PetSpider_0a6cbd9f-a8c5-4cbe-9f31-5a06d8db2554);

IF
ObjectFlagSet("RE_SpiderPet_BondedWithFighterTenant",(CHARACTERGUID)_SpiderPet,_)
AND
DB_IsPlayer(_Player)
THEN
SetFaction(CHARACTERGUID_S_RE_PetSpider_0a6cbd9f-a8c5-4cbe-9f31-5a06d8db2554,"FighterTeam");
DB_Dialogs(_SpiderPet,"RE_SpiderPet_BondedWithFighterTenant");
DB_AlivePets(_SpiderPet);
CharacterLookAt(_SpiderPet, CHARACTERGUID_S_RE_FighterTenant_42c6df62-f5c8-451d-a703-abef33e2072f);
ObjectSetFlag(_Player,"QuestUpdate_RE_FighterTenant_HatchedSpider");
Proc_StartDialog(1,"RE_AD_SpiderPet_HatchedComment",_SpiderPet);
Proc_StartDialog(1,"RE_AD_FighterTenant_GotSpiderComment",CHARACTERGUID_S_RE_FighterTenant_42c6df62-f5c8-451d-a703-abef33e2072f);
//END_REGION

IF
ObjectFlagSet("QuestUpdate_RE_FighterTenant_Happy",_Player,_)
THEN
SetVarInteger(CHARACTERGUID_S_RE_FighterTenant_42c6df62-f5c8-451d-a703-abef33e2072f, "StatusReApplyOnRemove", 0);
SetVarInteger(CHARACTERGUID_S_RE_FighterTenant_42c6df62-f5c8-451d-a703-abef33e2072f, "ForceStatusOnInit", 0);
RemoveStatus(CHARACTERGUID_S_RE_FighterTenant_42c6df62-f5c8-451d-a703-abef33e2072f, "ENRAGED");

IF
GlobalFlagSet("RE_MercsDefeated")
AND
CharacterIsDead(CHARACTERGUID_S_RE_FighterTenant_42c6df62-f5c8-451d-a703-abef33e2072f,0)
AND
CharacterIsDead(CHARACTERGUID_S_RE_PetSpider_0a6cbd9f-a8c5-4cbe-9f31-5a06d8db2554,0)
THEN
SetVarInteger(CHARACTERGUID_S_RE_FighterTenant_42c6df62-f5c8-451d-a703-abef33e2072f,"DisableAD",1);

IF
GameBookInterfaceClosed(ITEMGUID_S_RE_ArachnidsInRivellonMagazine_5c566595-84fd-489b-82f1-c5db61c547ee,_Player)
AND
QueryOnlyOnce("ReadArticle")
THEN
ObjectSetFlag(_Player,"RE_ReadArticle");
CharacterUnlockRecipe(_Player,"BOOK_Paper_Sheet_A_LOOT_Essence_Life_Step3_A_LOOT_Leg_Anthropod_A",1);

IF
ObjectFlagSet("RE_FighterRoomGotSearchPermission",(CHARACTERGUID)_Player,_)
THEN
TriggerClearItemOwner(TRIGGERGUID_S_RE_Ownership_FighterRoom_d2f36369-00a7-4fe9-a83e-50019f3a315b);

IF
GameBookInterfaceClosed(ITEMGUID_S_RE_Fighter_Diary_263ad6f3-11da-4887-9233-40ec508f3c0a,_Player)
THEN
ObjectSetFlag(_Player,"RE_KnowsFighterWantsPet");
ObjectSetFlag(_Player,"RE_ReadFighterDiary");

IF
ObjectFlagSet("RE_ReadFighterDiary",(CHARACTERGUID)_Player,_)
AND
ObjectGetFlag(_Player,"QuestUpdate_RE_GeomancerTenant_FoundRing",1)
THEN
ObjectSetFlag(_Player,"QuestUpdate_RE_GeomancerTenant_RealThief",1);

//REGION Fighter and Spider Madness
// 1. Spider bonded with Fighter and Fighter died - Spider is mad;
// 2. Spider bonded with Fighter and then died - Fighter is mad;
// 3. Fighter saw the player with Spider - Fighter is mad;
// 4. told Fighter there is no pet for him - Fighter is mad;

IF
CharacterDied(CHARACTERGUID_S_RE_FighterTenant_42c6df62-f5c8-451d-a703-abef33e2072f)
AND
CharacterIsDead(CHARACTERGUID_S_RE_PetSpider_0a6cbd9f-a8c5-4cbe-9f31-5a06d8db2554,0)
AND
ObjectGetFlag(CHARACTERGUID_S_RE_PetSpider_0a6cbd9f-a8c5-4cbe-9f31-5a06d8db2554,"RE_SpiderPet_BondedWithFighterTenant",1)
THEN
Proc_StartDialog(1,"RE_AD_SpiderPet_OwnerDied",CHARACTERGUID_S_RE_PetSpider_0a6cbd9f-a8c5-4cbe-9f31-5a06d8db2554);
Proc_DriveMad(CHARACTERGUID_S_RE_PetSpider_0a6cbd9f-a8c5-4cbe-9f31-5a06d8db2554);

IF
CharacterDied(CHARACTERGUID_S_RE_PetSpider_0a6cbd9f-a8c5-4cbe-9f31-5a06d8db2554)
AND
ObjectGetFlag(CHARACTERGUID_S_RE_PetSpider_0a6cbd9f-a8c5-4cbe-9f31-5a06d8db2554,"RE_SpiderPet_BondedWithPlayer",1)
AND
DB_IsPlayer(_Player)
THEN
NOT DB_BondedSpider(CHARACTERGUID_S_RE_PetSpider_0a6cbd9f-a8c5-4cbe-9f31-5a06d8db2554);
CharacterRemoveFromPlayerCharacter(CHARACTERGUID_S_RE_PetSpider_0a6cbd9f-a8c5-4cbe-9f31-5a06d8db2554,_Player);

IF
CharacterEnteredTrigger(_Player,TRIGGERGUID_S_RE_FighterTenantRoom_ac84c38f-c810-45b0-a964-e7f6241ebbbf)
AND
DB_IsPlayer(_Player)
AND
CharacterCanSee(CHARACTERGUID_S_RE_FighterTenant_42c6df62-f5c8-451d-a703-abef33e2072f,_Player,1)
AND
QRY_SpeakerIsAvailable(_Player)
AND
ObjectGetFlag(CHARACTERGUID_S_RE_PetSpider_0a6cbd9f-a8c5-4cbe-9f31-5a06d8db2554,"RE_SpiderPet_BondedWithPlayer",1)
THEN
Proc_StartDialog(1,"RE_AD_FighterTenant_Envy",CHARACTERGUID_S_RE_FighterTenant_42c6df62-f5c8-451d-a703-abef33e2072f);

IF
ObjectFlagSet("QuestUpdate_RE_FighterTenant_GoneMad",CHARACTERGUID_S_RE_FighterTenant_42c6df62-f5c8-451d-a703-abef33e2072f,_Inst)
THEN
DebugBreak("Gone MAD");
Proc_DriveMad(CHARACTERGUID_S_RE_FighterTenant_42c6df62-f5c8-451d-a703-abef33e2072f);//SetFaction(CHARACTERGUID_S_RE_FighterTenant_42c6df62-f5c8-451d-a703-abef33e2072f,"Evil NPC");

PROC
Proc_DriveMad((CHARACTERGUID)_Character)
THEN
SetVarInteger(CHARACTERGUID_S_RE_FighterTenant_42c6df62-f5c8-451d-a703-abef33e2072f,"StatusReApplyOnRemove",0);
RemoveStatus(_Character,"ENRAGED");
ApplyStatus(_Character,"MADNESS",-1.0,1);
SetFaction(_Character,"Evil NPC");
//END_REGION Fighter and Spider Madness
EXITSECTION

ENDEXITSECTION
ParentTargetEdge "ResidentsEvil"
