Version 1
SubGoalCombiner SGC_AND
INITSECTION
DB_Dialogs(CHARACTERGUID_S_RE_Wendy_9ae0e394-f0ea-4040-9c62-b08cd00e3904, "RE_Wendy");
DB_Dialogs(CHARACTERGUID_S_RE_PyromancerTenant_223377ec-3ae6-4d1e-9e2f-c7e2dfb3adbc,CHARACTERGUID_S_RE_GeomancerTenant_a694ba04-3404-4225-926a-2b4db8eb74ea,"RE_TenantsTerritoryArgument");
DB_Dialogs(CHARACTERGUID_S_RE_FighterTenant_42c6df62-f5c8-451d-a703-abef33e2072f, "RE_FighterTenant");
DB_Dialogs(CHARACTERGUID_S_RE_Merc_001_15555cae-b40b-4b58-a674-3bf7715fe554, CHARACTERGUID_S_RE_Merc_002_6283283e-3854-46f9-b723-80670a20a023, CHARACTERGUID_S_RE_Merc_003_3191f498-b1d1-4452-81d5-998adcd6585f, CHARACTERGUID_S_RE_Wendy_9ae0e394-f0ea-4040-9c62-b08cd00e3904,"RE_Lionel");
DB_Dialogs(CHARACTERGUID_S_RE_PetFireSlug_be69840c-5397-476f-9bc0-07d2cab60648,"RE_FireSlugPet");
DB_Dialogs(CHARACTERGUID_S_RE_PetAmphibian_ae984d09-ac51-424a-85ec-230a0145d9be,"RE_AmphibianPet");
DB_Dialogs(ITEMGUID_S_RE_HotelArea_Sign_d6729d8c-000b-4340-9398-c9a95e57cd76, "RE_HotelArea_Sign");
DB_Dialogs(CHARACTERGUID_Rat_2b49c49e-26e0-4afc-9969-8afb2ca0c1de, "RE_Rat");

DB_AD_Dialog(ITEMGUID_S_RE_HotelSign_9a509453-2a06-4269-b4af-9fd33dd2796b, "RE_AD_Hotel_Sign");
DB_AD_Dialog(ITEMGUID_S_RE_WipeFeet_Sign_c4210087-4e9a-48f1-bd2d-44f9d8fc5399, "RE_AD_WipeFeet_Sign");

// Main Quest
DB_Tenants((CHARACTERGUID)CHARACTERGUID_S_RE_GeomancerTenant_a694ba04-3404-4225-926a-2b4db8eb74ea);
DB_Tenants(CHARACTERGUID_S_RE_PyromancerTenant_223377ec-3ae6-4d1e-9e2f-c7e2dfb3adbc);
DB_Tenants(CHARACTERGUID_S_RE_FighterTenant_42c6df62-f5c8-451d-a703-abef33e2072f);
DB_Tenants(CHARACTERGUID_S_RE_RogueTenant_0047d1e0-c2e8-4076-809a-4e787ee1b96d);

DB_DeadTenants_JournalUpdates((CHARACTERGUID)CHARACTERGUID_S_RE_GeomancerTenant_a694ba04-3404-4225-926a-2b4db8eb74ea, "QuestUpdate_RE_GeomancerTenant_Dead");
DB_DeadTenants_JournalUpdates(CHARACTERGUID_S_RE_PyromancerTenant_223377ec-3ae6-4d1e-9e2f-c7e2dfb3adbc, "QuestUpdate_RE_PyromancerTenant_Dead");
DB_DeadTenants_JournalUpdates(CHARACTERGUID_S_RE_FighterTenant_42c6df62-f5c8-451d-a703-abef33e2072f, "QuestUpdate_RE_FighterTenant_Dead");
DB_DeadTenants_JournalUpdates(CHARACTERGUID_S_RE_RogueTenant_0047d1e0-c2e8-4076-809a-4e787ee1b96d, "QuestUpdate_RE_RogueTenant_Dead");

DB_HappyTenants_JournalUpdates((CHARACTERGUID)CHARACTERGUID_S_RE_GeomancerTenant_a694ba04-3404-4225-926a-2b4db8eb74ea,"QuestUpdate_RE_GeomancerTenant_Happy");
DB_HappyTenants_JournalUpdates(CHARACTERGUID_S_RE_PyromancerTenant_223377ec-3ae6-4d1e-9e2f-c7e2dfb3adbc,"QuestUpdate_RE_PyromancerTenant_Happy");
DB_HappyTenants_JournalUpdates(CHARACTERGUID_S_RE_FighterTenant_42c6df62-f5c8-451d-a703-abef33e2072f,"QuestUpdate_RE_FighterTenant_Happy");
DB_HappyTenants_JournalUpdates(CHARACTERGUID_S_RE_RogueTenant_0047d1e0-c2e8-4076-809a-4e787ee1b96d,"QuestUpdate_RE_RogueTenant_Happy");

DB_AlivePets((CHARACTERGUID)CHARACTERGUID_S_RE_PetFireSlug_be69840c-5397-476f-9bc0-07d2cab60648);
DB_AlivePets(CHARACTERGUID_S_RE_PetAmphibian_ae984d09-ac51-424a-85ec-230a0145d9be);
// DB_AlivePets(CHARACTERGUID_S_RE_PetSpider_0a6cbd9f-a8c5-4cbe-9f31-5a06d8db2554); -- after hatching 

DB_HasStoryEvent(ITEMGUID_S_RE_GeomancerRing_b3924e0c-6bfd-41b9-a7ac-6ef4125899db,"RE_HasGeomancerRing");
DB_GiveItemToEvent(ITEMGUID_S_RE_GeomancerRing_b3924e0c-6bfd-41b9-a7ac-6ef4125899db,"RE_GiveGeomancerRing");
DB_HasStoryEvent(ITEMGUID_S_RE_KeyToRogueRoomSpare_4e091364-325c-4b7c-82a5-df05e2f7ab51,"RE_HasKeyToRogueRoomSpare");
DB_GiveItemToEvent(ITEMGUID_S_RE_KeyToRogueRoomSpare_4e091364-325c-4b7c-82a5-df05e2f7ab51,"RE_GiveKeyToRogueRoomSpare");
DB_HasStoryEvent(ITEMGUID_S_RE_KeyToRogueRoomMain_4bf955d3-18d8-4648-985c-65f09ff754b0,"RE_HasKeyToRogueRoomMain");
DB_GiveItemToEvent(ITEMGUID_S_RE_KeyToRogueRoomMain_4bf955d3-18d8-4648-985c-65f09ff754b0,"RE_GiveKeyToRogueRoomMain");
DB_HasStoryEvent(ITEMGUID_S_RE_Fighter_Diary_263ad6f3-11da-4887-9233-40ec508f3c0a,"RE_HasFighterDiary");
DB_GiveItemToEvent(ITEMGUID_S_RE_Fighter_Diary_263ad6f3-11da-4887-9233-40ec508f3c0a,"RE_GiveFighterDiary");

DB_StartItems("EQ_Armor_Leather_A_Helmet_A_ebeab7a2-9ed4-4c84-9a68-2ac6d4614b6d");
DB_StartItems("EQ_Armor_Leather_Arms_A_fe0754e3-5f0b-409e-a856-31e646201ee4");
DB_StartItems("EQ_Armor_Leather_Legs_A_125974b5-0322-4167-bce5-264f251f9d5f");
DB_StartItems("EQ_Armor_Leather_LowerBody_A_5d6862f9-6963-40bf-9acb-42ec7fb0e1a1");
DB_StartItems("EQ_Armor_Leather_UpperBody_A_067a9f39-482e-49aa-b1b3-bc12f774b851");
DB_StartItems("FUR_Humans_Camping_Sleepingbag_B_4d7216c9-c21e-4ab0-b98e-97d744798912");
DB_StartItems("GRN_Grenade_BlessedWaterBalloon_A_40ce7ae0-d0a9-4d76-b638-4bbd2814075f");
DB_StartItems("GRN_Grenade_Molotov_A_5208b121-64fa-4704-8924-c61c576e1ac5");
DB_StartItems("CON_Drink_Cup_A_Lemonade_7f6bdd25-5511-41c9-ac4d-97d2aba71495");
DB_StartItems("CON_Food_Dinner_A_70e3b498-e2af-44f1-aedb-397ec186019a");

DB_AlignmentChanges((CHARACTERGUID)S_RE_FighterTenant_42c6df62-f5c8-451d-a703-abef33e2072f,"QuestUpdate_RE_FighterTenant_Happy");
DB_AlignmentChanges(S_RE_PetSpider_0a6cbd9f-a8c5-4cbe-9f31-5a06d8db2554,"QuestUpdate_RE_FighterTenant_Happy");
DB_AlignmentChanges(S_RE_PetFireSlug_be69840c-5397-476f-9bc0-07d2cab60648,"QuestUpdate_RE_PyromancerTenant_Happy");
DB_AlignmentChanges(S_RE_PyromancerTenant_223377ec-3ae6-4d1e-9e2f-c7e2dfb3adbc,"QuestUpdate_RE_PyromancerTenant_Happy");
DB_AlignmentChanges(S_RE_GeomancerTenant_a694ba04-3404-4225-926a-2b4db8eb74ea,"QuestUpdate_RE_GeomancerTenant_Happy");
DB_AlignmentChanges(S_RE_PetAmphibian_ae984d09-ac51-424a-85ec-230a0145d9be,"QuestUpdate_RE_GeomancerTenant_Happy");

DB_PetDiedDialogs((CHARACTERGUID)CHARACTERGUID_S_RE_PetAmphibian_ae984d09-ac51-424a-85ec-230a0145d9be,(CHARACTERGUID)CHARACTERGUID_S_RE_GeomancerTenant_a694ba04-3404-4225-926a-2b4db8eb74ea,"RE_AD_GeomancerTenant_PetDied");
DB_PetDiedDialogs(CHARACTERGUID_S_RE_PetFireSlug_be69840c-5397-476f-9bc0-07d2cab60648,CHARACTERGUID_S_RE_PyromancerTenant_223377ec-3ae6-4d1e-9e2f-c7e2dfb3adbc,"RE_AD_PyromancerTenant_PetDied");
DB_PetDiedDialogs(CHARACTERGUID_S_RE_PetSpider_0a6cbd9f-a8c5-4cbe-9f31-5a06d8db2554,CHARACTERGUID_S_RE_FighterTenant_42c6df62-f5c8-451d-a703-abef33e2072f,"RE_AD_FighterTenant_PetDied");

SetOnStage(ITEMGUID_S_RE_PyromancerTenantHatch_40bea39a-64a6-44cc-9381-c086cbf65783,0);
SetOnStage(ITEMGUID_S_RE_PyromancerTenantCarpetFolded_57df18b8-8c30-47c1-8f12-e8de8edcd0ed,0);
KBSECTION
//REGION Initialization 
// simple solution to override initial equipment + create initial journal updates
IF
GameStarted("ResidentsEvil",_)
AND
DB_IsPlayer(_Player)
AND
QueryOnlyOnce("ResidentsEvil_Init")
THEN
TimerLaunch("InitTimer",50);

IF
TimerFinished("InitTimer")
AND
DB_IsPlayer(_Player)
THEN
Proc_AddStartItems(_Player);
Proc_AddStartInfo(_Player);

PROC
Proc_AddStartItems((CHARACTERGUID)_Player)
THEN
CharacterAddGold(_Player,700);

PROC
Proc_AddStartItems((CHARACTERGUID)_Player)
AND
DB_StartItems((STRING)_Item)
AND
DB_IsPlayer(_Player)
THEN
ItemTemplateAddTo(_Item, _Player, 1);
Proc_EquipItem(_Item,_Player);

PROC
Proc_EquipItem((STRING)_ItemTemplate,(CHARACTERGUID)_Player)
AND
GetItemForItemTemplateInInventory(_Player,_ItemTemplate,_Item)
THEN
CharacterEquipItem(_Player,_Item);

PROC
Proc_AddStartInfo((CHARACTERGUID)_Player)
THEN
Proc_StartDialog(1,"RE_AD_Player_FirstComment",_Player);
ObjectSetFlag(_Player,"QuestUpdate_CORE_ResidentsEvil_InitialSetup");
ObjectSetFlag(_Player,"QuestUpdate_Notes_InitialEntry");
//END_REGION

IF
ItemAddedToCharacter(_Item,_Player)
AND
DB_IsPlayer(_Player)
AND
ItemIsStoryItem(_Item,1)
THEN
Proc_StartDialog(1,"RE_AD_StoryItemComments",_Player); // otherwise voice barks like "Lucky find" don't work

//REGION Tenants Argument
IF 
CharacterLeftTrigger(_Player,TRIGGERGUID_S_RE_TenantsTerritoryArgumentAreaTrigger_8b55e946-5e80-4ff1-90df-71ed4e627413)
AND
QueryOnlyOnce("TenantsArgumentIsOver")
THEN
Proc_TenantsArgumentIsOver();

IF
DialogEnded("RE_TenantsTerritoryArgument",_Inst)
AND
QueryOnlyOnce("TenantsArgumentIsOver")
THEN
Proc_TenantsArgumentIsOver();

PROC
Proc_TenantsArgumentIsOver()
THEN
GlobalSetFlag("GLO_TenantsTerritoryArgumentIsOver");
PROC_RemoveDialogFromCharacter(CHARACTERGUID_S_RE_GeomancerTenant_a694ba04-3404-4225-926a-2b4db8eb74ea);
PROC_RemoveDialogFromCharacter(CHARACTERGUID_S_RE_PyromancerTenant_223377ec-3ae6-4d1e-9e2f-c7e2dfb3adbc);
DB_Dialogs(CHARACTERGUID_S_RE_GeomancerTenant_a694ba04-3404-4225-926a-2b4db8eb74ea,"RE_GeomancerTenant");
DB_Dialogs(CHARACTERGUID_S_RE_PyromancerTenant_223377ec-3ae6-4d1e-9e2f-c7e2dfb3adbc,"RE_PyromancerTenant");

IF
CharacterEnteredTrigger(_Player,TRIGGERGUID_S_RE_PlayerEnteredHotel_EventTrigger_33e5e9c3-5f6b-445f-8966-e9adaf426249)
AND
DB_IsPlayer(_Player)
AND
QueryOnlyOnce("RE_PlayerEnteredHotel")
THEN
Proc_StartDialog(1,"RE_AD_TenantsTerritoryArgument",CHARACTERGUID_S_RE_PyromancerTenant_223377ec-3ae6-4d1e-9e2f-c7e2dfb3adbc,CHARACTERGUID_S_RE_GeomancerTenant_a694ba04-3404-4225-926a-2b4db8eb74ea);
GlobalSetFlag("GLO_PlayerEnteredHotel");
//END_REGION TenantsArgument

//REGION Main Quest Updates
// there are four tenants and we could kill / spare any of them
IF
CharacterDied(_Character)
AND
DB_DeadTenants_JournalUpdates(_Character, _QuestUpdate)
AND
DB_HappyTenants_JournalUpdates(_Character, _QuestUpdateToRemove)
AND
DB_IsPlayer(_Player)
THEN
NOT DB_HappyTenants(_Character);
ObjectClearFlag(_Player, _QuestUpdateToRemove);
ObjectSetFlag(_Player,_QuestUpdate,0);
DB_DeadTenants(_Character);
DB_PacifiedTenants(_Character);

IF
ObjectFlagSet(_Flag, (CHARACTERGUID)_Player, _ID)
AND
DB_HappyTenants_JournalUpdates(_Character, _Flag)
AND
DB_IsPlayer(_Player)
THEN
DB_HappyTenants(_Character);
DB_PacifiedTenants(_Character);
CharacterAddAttitudeTowardsPlayer(_Character,_Player,25);

IF
DB_PacifiedTenants(_Character)
AND
NOT QRY_RE_CORE_ResidentsEvil_SomeTenantsArentPacified()
THEN
PROC_RE_CORE_ResidentsEvil_CompleteTask();

QRY
QRY_RE_CORE_ResidentsEvil_SomeTenantsArentPacified()
AND
DB_Tenants(_Character)
AND
NOT DB_PacifiedTenants(_Character)
THEN
DB_NOOP(1);

PROC
PROC_RE_CORE_ResidentsEvil_CompleteTask()
AND
NOT QRY_RE_CORE_ResidentsEvil_SomeTenantsArentHappy()
AND
DB_IsPlayer(_Player)
THEN
ObjectSetFlag(_Player, "QuestUpdate_CORE_ResidentsEvil_AllTenantsAreHappy");
GlobalSetFlag("GLO_ResidentsEvilQuestCompleted");

PROC
PROC_RE_CORE_ResidentsEvil_CompleteTask()
AND
QRY_RE_CORE_ResidentsEvil_SomeTenantsArentHappy()
AND
QRY_RE_CORE_ResidentsEvil_SomeTenantsArentDead()
AND
DB_IsPlayer(_Player)
THEN
ObjectClearFlag(_Player, "QuestUpdate_CORE_ResidentsEvil_AllTenantsAreHappy");
ObjectSetFlag(_Player, "QuestUpdate_CORE_ResidentsEvil_AllTenantsAreNegotiated");
GlobalSetFlag("GLO_ResidentsEvilQuestCompleted");

PROC
PROC_RE_CORE_ResidentsEvil_CompleteTask()
AND
NOT QRY_RE_CORE_ResidentsEvil_SomeTenantsArentDead()
AND
DB_IsPlayer(_Player)
THEN
ObjectClearFlag(_Player, "QuestUpdate_CORE_ResidentsEvil_AllTenantsAreNegotiated");
ObjectSetFlag(_Player, "QuestUpdate_CORE_ResidentsEvil_AllTenantsAreDead");
GlobalSetFlag("GLO_ResidentsEvilQuestCompleted");

QRY
QRY_RE_CORE_ResidentsEvil_SomeTenantsArentHappy()
AND
DB_Tenants(_Character)
AND
NOT
DB_HappyTenants(_Character)
THEN
DB_NOOP(1);

QRY
QRY_RE_CORE_ResidentsEvil_SomeTenantsArentDead()
AND
DB_Tenants(_Character)
AND
NOT DB_DeadTenants(_Character)
THEN
DB_NOOP(1);

IF
CharacterDied(CHARACTERGUID_S_RE_Wendy_9ae0e394-f0ea-4040-9c62-b08cd00e3904)
AND
DB_IsPlayer(_Player)
AND
ObjectGetFlag(_Player,"QuestUpdate_CORE_ResidentsEvil_TaskCompleted",0)
THEN
ObjectSetFlag(_Player,"QuestUpdate_CORE_ResidentsEvil_TaskFailed");
ObjectSetFlag(_Player,"QuestUpdate_RE_GeomancerTenant_Closed");
ObjectSetFlag(_Player,"QuestUpdate_RE_PyromancerTenant_Closed");
ObjectSetFlag(_Player,"QuestUpdate_RE_RogueTenant_Closed");
ObjectSetFlag(_Player,"QuestUpdate_RE_FighterTenant_Closed");

IF
ObjectFlagSet(_Flag,(CHARACTERGUID)_Player,_)
AND
DB_IsPlayer(_Player)
AND
DB_AlignmentChanges(_Char,_Flag)
THEN
SetFaction(_Char,"NeutralAlly"); // when tenants became happy they get common alignment, so they could help each other

IF
CharacterEnteredTrigger(_Player, TRIGGERGUID_S_RE_GreetingTrigger_43e57bb7-90d3-4688-91ce-0452559e1f4d)
AND
DB_IsPlayer(_Player)
AND
ObjectGetFlag(_Player,"RE_ClientHasMet",0)
AND
CharacterCanSee(CHARACTERGUID_S_RE_Wendy_9ae0e394-f0ea-4040-9c62-b08cd00e3904,_Player,1)
THEN
CharacterLookAt(CHARACTERGUID_S_RE_Wendy_9ae0e394-f0ea-4040-9c62-b08cd00e3904, _Player);
Proc_StartDialog(0,"RE_Wendy",CHARACTERGUID_S_RE_Wendy_9ae0e394-f0ea-4040-9c62-b08cd00e3904, _Player);

IF
CharacterEnteredTrigger(_Player, TRIGGERGUID_S_RE_GreetingTrigger_43e57bb7-90d3-4688-91ce-0452559e1f4d)
AND
DB_IsPlayer(_Player)
AND
QueryOnlyOnce("RE_FinalAutoSave")
AND
GlobalGetFlag("GLO_ResidentsEvilQuestCompleted",1)
THEN
AutoSave();

//END_REGION Main Quest

//REGION Final Combat
IF
ObjectFlagSet("RE_GotMoney", (CHARACTERGUID)_Player, _ID)
AND
DB_IsPlayer(_Player)
THEN
CharacterAddGold(_Player,1000);
ItemTemplateAddTo("ITEMGUID_Scroll_Skill_Water_LightningBolt_31798bd0-aff8-49b4-adec-dbe5c5390f80",_Player,1);
ItemTemplateAddTo("ITEMGUID_Scroll_Skill_Water_HailStrike_9644aedc-da69-4073-8993-8c87dad37acd",_Player,1);
ItemTemplateAddTo("ITEMGUID_Scroll_Skill_Water_Rain_35040ba8-1f97-418c-b4a2-3af74e14bc36",_Player,1);

IF
ObjectFlagSet("RE_GotMoney",(CHARACTERGUID)_Player,_ID)
THEN
DB_StartFinalCombat(_ID);

IF
DialogEnded("RE_Wendy",_ID)
AND
DB_StartFinalCombat(_ID)
THEN
Proc_StartFinalCombatDialog();

PROC
Proc_StartFinalCombatDialog()
AND
DB_IsPlayer(_Player)
AND
GetPosition(CHARACTERGUID_S_RE_Merc_001_15555cae-b40b-4b58-a674-3bf7715fe554, _X1, _Y1, _Z1)
AND
GetPosition(CHARACTERGUID_S_RE_Merc_002_6283283e-3854-46f9-b723-80670a20a023, _X2, _Y2, _Z2)
AND
GetPosition(CHARACTERGUID_S_RE_Merc_003_3191f498-b1d1-4452-81d5-998adcd6585f, _X3, _Y3, _Z3)
THEN
PlayEffectAtPosition("RS3_FX_GP_ScriptedEvent_Teleport_GenericSmoke_01", _X1, _Y1, _Z1);
CharacterAppear(CHARACTERGUID_S_RE_Merc_001_15555cae-b40b-4b58-a674-3bf7715fe554, 1,"");
PlayEffectAtPosition("RS3_FX_GP_ScriptedEvent_Teleport_GenericSmoke_01", _X2, _Y2, _Z2);
CharacterAppear(CHARACTERGUID_S_RE_Merc_002_6283283e-3854-46f9-b723-80670a20a023, 1,"");
PlayEffectAtPosition("RS3_FX_GP_ScriptedEvent_Teleport_GenericSmoke_01", _X3, _Y3, _Z3);
CharacterAppear(CHARACTERGUID_S_RE_Merc_003_3191f498-b1d1-4452-81d5-998adcd6585f, 1,"");
ProcForceStopDialog(CHARACTERGUID_S_RE_Wendy_9ae0e394-f0ea-4040-9c62-b08cd00e3904);
Proc_StartDialog(0, "RE_Lionel", 
					CHARACTERGUID_S_RE_Merc_001_15555cae-b40b-4b58-a674-3bf7715fe554, 
					CHARACTERGUID_S_RE_Merc_002_6283283e-3854-46f9-b723-80670a20a023,
					CHARACTERGUID_S_RE_Merc_003_3191f498-b1d1-4452-81d5-998adcd6585f,
					CHARACTERGUID_S_RE_Wendy_9ae0e394-f0ea-4040-9c62-b08cd00e3904,
					_Player);

IF
DialogEnded("RE_Lionel", _ID)
AND
DB_IsPlayer(_Player)
AND
DB_RE_Mercs(_Char)
THEN
SetFaction(_Char, "Evil NPC");
CharacterSetRelationFactionToIndivFaction("FighterTeam",_Player,100);
CharacterSetRelationFactionToIndivFaction("Owner",_Player,100);
ObjectSetFlag(_Player,"QuestUpdate_CORE_ResidentsEvil_MercsAttack");

IF
ObjectEnteredCombat(CHARACTERGUID_S_RE_Wendy_9ae0e394-f0ea-4040-9c62-b08cd00e3904,_)
AND
QueryOnlyOnce("RE_FinalFight")
AND
DB_IsPlayer(_Player)
AND
ObjectGetFlag((CHARACTERGUID)_Player, "RE_GotMoney",1)
THEN
SetScriptFrame(CHARACTERGUID_S_RE_Wendy_9ae0e394-f0ea-4040-9c62-b08cd00e3904,"CombatInit");

IF
GlobalFlagSet("RE_CalledForHelp")
THEN
Proc_RE_CryForHelp();

PROC
Proc_RE_CryForHelp()
AND
DB_IsPlayer(_Player)
AND
ObjectGetFlag(_Player, "QuestUpdate_RE_FighterTenant_Happy",1)
THEN
TeleportTo(CHARACTERGUID_S_RE_FighterTenant_42c6df62-f5c8-451d-a703-abef33e2072f,TRIGGERGUID_S_RE_FighterTeleportTrigger_91183969-edf3-4d92-a2ac-f23559f108f7,"",0);
PlayEffect(CHARACTERGUID_S_RE_FighterTenant_42c6df62-f5c8-451d-a703-abef33e2072f, "RS3_FX_GP_ScriptedEvent_Teleport_GenericSmoke_01");
Proc_StartDialog(1,"RE_FighterTenant_FinalFightComment",CHARACTERGUID_S_RE_FighterTenant_42c6df62-f5c8-451d-a703-abef33e2072f);
Proc_RE_InvitePetSpider();

PROC
Proc_RE_InvitePetSpider()
AND
DB_AlivePets(CHARACTERGUID_S_RE_PetSpider_0a6cbd9f-a8c5-4cbe-9f31-5a06d8db2554)
THEN
TeleportTo(CHARACTERGUID_S_RE_PetSpider_0a6cbd9f-a8c5-4cbe-9f31-5a06d8db2554,TRIGGERGUID_S_RE_SpiderPetTeleportTrigger_481fd863-a6a9-4905-8789-eb19b94cdbbe,"",0);
PlayEffect(CHARACTERGUID_S_RE_PetSpider_0a6cbd9f-a8c5-4cbe-9f31-5a06d8db2554, "RS3_FX_GP_ScriptedEvent_Teleport_GenericSmoke_01");
//END_REGION Final Combat

//REGION GameEnd
IF
CharacterDied((CHARACTERGUID)_Player)
AND
DB_IsPlayer(_Player)
THEN
GlobalSetFlag("RE_GameEnd");

IF
GlobalFlagSet("RE_GameEnd")
THEN
GameEnd();
//END_REGION GameEnd

//REGION AbandonedPartPassage
IF
CharacterUsedItem(_Player, ITEMGUID_S_RE_DoorToAbandonedPart_eec1ae4e-1618-4465-b85f-db66ac9ddc03)
AND
ItemIsLocked(ITEMGUID_S_RE_DoorToAbandonedPart_eec1ae4e-1618-4465-b85f-db66ac9ddc03,0)
THEN
TeleportTo(_Player, TRIGGERGUID_S_RE_AbandonedPartMainEntranceTrigger_aae0be54-094e-4861-b3ce-a179fa692188);

IF
CharacterUsedItem(_Player, ITEMGUID_S_RE_AbandonedPartMainEntrance_99e89d1b-6402-4862-a71e-e111d631e982)
AND
ItemIsLocked(ITEMGUID_S_RE_DoorToAbandonedPart_eec1ae4e-1618-4465-b85f-db66ac9ddc03,0)
THEN
TeleportTo(_Player, TRIGGERGUID_S_RE_DoorToAbandonedPartTrigger_c7524cbb-8002-42e9-be42-3105911302fb);
//END_REGION

//REGION Hotel Entrance
IF
CharacterUsedItem(_Player, ITEMGUID_S_RE_Cozy_Cave_Entrance_14e1c5f3-4adf-451a-a4ba-c8772f705183)
AND
ObjectGetFlag(_Player,"RE_ClientHasMet",1)
THEN
TeleportTo(_Player, TRIGGERGUID_S_RE_Hotel_Interior_TeleportTrigger_2738fba0-b92f-4848-89d4-a3cea2a9fa5e);

IF
CharacterUsedItem(_Player, ITEMGUID_S_RE_Cozy_Cave_Entrance_14e1c5f3-4adf-451a-a4ba-c8772f705183)
AND
ObjectGetFlag(_Player,"RE_ClientHasMet",0)
AND
DB_IsPlayer(_Player)
THEN
Proc_StartDialog(0,"RE_Wendy",CHARACTERGUID_S_RE_Wendy_9ae0e394-f0ea-4040-9c62-b08cd00e3904, _Player);

IF
CharacterUsedItem(_Player, ITEMGUID_S_RE_AbandonedPartMainEntrance_99e89d1b-6402-4862-a71e-e111d631e982)
AND
ItemIsLocked(ITEMGUID_S_RE_DoorToAbandonedPart_eec1ae4e-1618-4465-b85f-db66ac9ddc03,1)
THEN
Proc_StartDialog(1,"RE_PassagedBlocked_Comment",_Player);
//END_REGION

//REGION Pets
IF
CharacterDied(_Char)
AND
DB_AlivePets(_Char)
THEN
NOT DB_AlivePets(_Char);

IF
CharacterDied(_Pet)
AND
DB_PetDiedDialogs(_Pet,_Owner,_Dialog)
AND
CharacterIsDead(_Owner,0)
THEN
Proc_StartDialog(1,_Dialog,_Owner);

IF
ObjectEnteredCombat((CHARACTERGUID)_NPC, _ID)
AND
DB_Tenants(_NPC)
AND
DB_IsPlayer(_Player)
AND
DB_CombatCharacters(_Player, _ID)
AND
CharacterIsEnemy(_Player, _NPC, 1)
AND
DB_BondedSpider(_PetSpider)
AND
GetFaction(_NPC,_Faction)
THEN
CharacterSetRelationFactionToIndivFaction(_Faction, _PetSpider, -100);
//END_REGION

//REGION pet spider fix - after loading the game the pet spider is not in the party anymore
IF
GameStarted("ResidentsEvil",_)
AND
DB_IsPlayer(_Player)
THEN
TimerLaunch("FixSpiderTimer",50);

IF
TimerFinished("FixSpiderTimer")
AND
DB_IsPlayer(_Player)
THEN
Proc_FixPetSpider(_Player);

PROC
Proc_FixPetSpider((CHARACTERGUID)_Player)
AND
DB_BondedSpider(_PetSpider)
THEN
CharacterRemoveFromPlayerCharacter(_PetSpider,_Player);
CharacterAddToPlayerCharacter(_PetSpider,_Player);
//END_REGION
EXITSECTION

ENDEXITSECTION
ParentTargetEdge "ResidentsEvil"
